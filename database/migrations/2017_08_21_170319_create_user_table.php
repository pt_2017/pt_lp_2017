<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 150);
            $table->string('username', 100)->unique();
            $table->string('email',100)->unique();
            $table->string('password',1000);
            $table->string('matricule', 100)->unique();
            $table->date('dob');
            $table->string('adress', 100);
            $table->string('phone', 20);
            $table->integer('manager_dep')->unsigned()->nullable();
            $table->boolean('active');
            $table->integer('role_id')->unsigned();
            $table->string('photo',100)->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('manager_dep')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
