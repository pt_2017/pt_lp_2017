<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassesTable extends Migration
{
    public function up()
    {
        Schema::create('classes', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('niveau');
            $table->string('libelle', 100)->unique();
            $table->string('filiere', 100);
            $table->integer('departement_id')->unsigned();
            $table->timestamps();
            $table->foreign('departement_id')->references('id')->on('departements');
        });
    }

    public function down()
    {
        Schema::drop('classes');
    }
}
