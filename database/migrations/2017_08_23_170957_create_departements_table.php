<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDepartementsTable extends Migration
{
    public function up()
    {
        Schema::create('departements', function(Blueprint $table) {
            $table->increments('id');
            $table->string('libelle', 100)->unique();
            $table->integer('manager_dep')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('manager_dep')->references('manager_dep')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('departements');
    }
}
