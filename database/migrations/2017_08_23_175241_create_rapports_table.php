<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRapportsTable extends Migration
{
    public function up()
    {
        Schema::create('rapports', function(Blueprint $table) {
            $table->increments('id');
            $table->string('reference', 100);
            $table->integer('document_id')->unsigned();
            $table->timestamps();
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    public function down()
    {
        Schema::drop('rapports');
    }
}
