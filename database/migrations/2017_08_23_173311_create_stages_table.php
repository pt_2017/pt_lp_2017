<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStagesTable extends Migration
{
    public function up()
    {
        Schema::create('stages', function(Blueprint $table) {
            $table->increments('id');
            $table->string('entreprise', 100);
            $table->string('adresse', 100);
            $table->string('telephone', 20);
            $table->string('email', 100);
            $table->integer('projet_id')->unsigned();
            $table->timestamps();
            $table->foreign('projet_id')->references('id')->on('projets');

        });
    }

    public function down()
    {
        Schema::drop('stages');
    }
}
