<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetsTable extends Migration
{
    public function up()
    {
        Schema::create('projets', function(Blueprint $table) {
            $table->increments('id');
            $table->text('canevas');
            $table->float('note_rapport', 4,2);
            $table->float('note_soutenance', 4,2);
            $table->integer('theme_id')->unsigned();
            $table->timestamps();
            $table->foreign('theme_id')->references('id')->on('themes');

        });
    }

    public function down()
    {
        Schema::drop('projets');
    }
}
