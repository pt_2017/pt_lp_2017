<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTachesTable extends Migration
{
    public function up()
    {
        Schema::create('taches', function(Blueprint $table) {
            $table->increments('id');
            $table->string('libelle', 100);
            $table->text('description');
            $table->text('remarque');
            $table->text('notes');
            $table->integer('document_id')->unsigned();
            $table->timestamps();
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    public function down()
    {
        Schema::drop('taches');
    }
}
