<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProjetTable extends Migration
{
    public function up()
    {
        Schema::create('user_projet', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('projet_id')->unsigned()->nullable();
            $table->integer('encadreur_id')->unsigned()->nullable();
            $table->integer('etudiant_id')->unsigned()->nullable();
            $table->integer('jury_id')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('projet_id')->references('id')->on('projets');
            $table->foreign('encadreur_id')->references('id')->on('users');
            $table->foreign('etudiant_id')->references('id')->on('roles');
            $table->foreign('jury_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('user_projet');
    }
}
