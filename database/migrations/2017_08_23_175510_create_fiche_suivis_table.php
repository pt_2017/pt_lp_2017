<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFicheSuivisTable extends Migration
{
    public function up()
    {
        Schema::create('fiche_suivis', function(Blueprint $table) {
            $table->increments('id');
            $table->string('ref', 100);
            $table->integer('document_id')->unsigned();
            $table->timestamps();
            $table->foreign('document_id')->references('id')->on('documents');
        });
    }

    public function down()
    {
        Schema::drop('fiche_suivis');
    }
}
