<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserClasseTable extends Migration
{
    public function up()
    {
        Schema::create('user_classe', function(Blueprint $table) {
            $table->increments('id');
            $table->string('annee_academique', 20);
            $table->integer('user_id')->unsigned();
            $table->integer('classe_id')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('classe_id')->references('id')->on('classes');
        });
    }

    public function down()
    {
        Schema::drop('user_classe');
    }
}
