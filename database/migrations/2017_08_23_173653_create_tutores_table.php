<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTutoresTable extends Migration
{
    public function up()
    {
        Schema::create('tutores', function(Blueprint $table) {
            $table->increments('id');
            $table->string('site', 100);
            $table->integer('projet_id')->unsigned();
            $table->timestamps();
            $table->foreign('projet_id')->references('id')->on('projets');
        });
    }

    public function down()
    {
        Schema::drop('tutores');
    }
}
