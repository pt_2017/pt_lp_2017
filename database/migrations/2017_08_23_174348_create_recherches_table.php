<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecherchesTable extends Migration
{
    public function up()
    {
        Schema::create('recherches', function(Blueprint $table) {
            $table->increments('id');
            $table->string('domaine', 100);
            $table->integer('projet_id')->unsigned();
            $table->timestamps();
            $table->foreign('projet_id')->references('id')->on('projets');
        });
    }

    public function down()
    {
        Schema::drop('recherches');
    }
}
