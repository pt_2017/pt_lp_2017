<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>'djika blandine',
            'username'=>'blandine',
            'email'=>'medjika@yahoo.fr',
            'password'=>bcrypt('123456'),
            'remember_token'=>str_random(10),
            'matricule'=>'ISTDI09E0777',
            'dob'=>'1979-08-22',
            'adress'=>'LogPom',
            'phone'=>'697771524',
            'manager_dep'=>null,
            'photo'=>null,
            'active'=>1,
            'role_id'=>1,

        ]);
    }
}
