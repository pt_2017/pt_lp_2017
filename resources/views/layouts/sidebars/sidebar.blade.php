<!--sidebar start-->
<aside>
    <div id="sidebar"  class="nav-collapse ">
        <!-- sidebar menu start-->
        <ul class="sidebar-menu">
            <li class="active">
                <a class="" href="{{route('dashboard')}}">
                    <i class="icon_house_alt"></i>
                    <span>Tableau de bord</span>
                </a>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="icon_document_alt"></i>
                    <span>Gérer Thèmes</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="form_component.html">Demander Thèmes</a></li>
                    <li><a class="" href="form_validation.html">Envoyer Thèmes</a></li>
                    <li><a class="" href="form_validation.html">Consulter Thèmes</a></li>
                </ul>
            </li>
            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="fa fa-tasks"></i>
                    <span>Gérer Projets</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="general.html">Recherches</a></li>
                    <li><a class="" href="buttons.html">Tutorés</a></li>
                    <li><a class="" href="grids.html">Stages</a></li>
                </ul>
            </li>
            <li>
                <a class="" href="widgets.html">
                    <i class="fa fa-book"></i>
                    <span>Archives</span>
                </a>
            </li>

            <li class="sub-menu">
                <a href="javascript:;" class="">
                    <i class="fa fa-cog"></i>
                    <span>Paramètres</span>
                    <span class="menu-arrow arrow_carrot-right"></span>
                </a>
                <ul class="sub">
                    <li><a class="" href="login.html"><span>Departement</span></a></li>
                    <li><a class="" href="blank.html">Filières</a></li>
                    <li><a class="" href="404.html">Utilisateurs</a></li>
                </ul>
            </li>

            <li>
                <a class="" href="chart-chartjs.html">
                    <i class="fa fa-user"></i>
                    <span>A propos de nous</span>

                </a>
            </li>

        </ul>
        <!-- sidebar menu end-->
    </div>
</aside>
<!--sidebar end-->